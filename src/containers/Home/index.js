import React from 'react'
import {
  TransitionContainer,
  Container
} from 'components'
import {
  ROUTE_PATH,
  redirect
} from 'helpers'

export class HomeContainer extends React.Component {
  render() {
    return (
      <TransitionContainer
        // motion='overlap-from'
      >
        <Container width='desktop'>
          <div style={{ width: '100%', height: '100vh', backgroundColor: 'lightgray' }}
            onClick={() => {
              redirect(ROUTE_PATH.DETAIL.LINK)
            }}
          >
            Home
          </div>
        </Container>
      </TransitionContainer>
    )
  }
}
