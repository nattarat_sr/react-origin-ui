import React from 'react'
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom'
import {
  DeviceDetect
} from 'components'
import {
  RouteContainer
} from 'containers'
import {
  Provider
} from 'context'

export default class App extends React.Component {
  render() {
    return (
      <DeviceDetect>
        <Provider app={this}>
          <Router>
            <Route path='/' component={RouteContainer} />
          </Router>
        </Provider>
      </DeviceDetect>
    );
  }
}
