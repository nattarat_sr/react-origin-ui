import styled from 'styled-components'
import {
  default as VARIABLES
} from '../../themes/styles/bases/variables'
// import {
//   default as TYPOGRAPHYS
// } from '../../themes/styles/bases/typographys'
// import {
//   default as MIXINS
// } from '../../themes/styles/helpers/mixins'
// import {
//   default as UTILITIES
// } from '../../themes/styles/helpers/utilities'
// import {
//   // Example using:
//   // background: url(${CONTENTS['image-sample.svg']});
//   CONTENTS,
//   ICONS,
//   LOGOS,
//   SHAREDS,
//   DOCUMENTS
// } from '../../themes'

// Wrapper
// ============================================================
export const PlaceholderWrapper = styled.div`
  /* Parent
  ------------------------------- */
  margin-right: auto;
  margin-left: auto;

  /* Childrens
  ------------------------------- */
  .placeholder-content {}

  /* Modifiers
  ------------------------------- */
  /* Size */
  &.is-size-mobile-lg {
    max-width: ${VARIABLES.BREAKPOINTS.MOBILE_LG};
  }

  /* Media queries
  ------------------------------- */
`
