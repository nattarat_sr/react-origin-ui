Default:

```jsx
<Image
  srcPlaceholder={require('./images/default-banner.svg')}
  src={require('./images/sample-banner.jpg')}
  alt='Sample Banner'
/>
<div style={{ paddingTop: '5px' }}>Default image component is fluid(width 100 %) and 16:9 ratio</div>
```

Placeholder:

```jsx
<Image
  srcPlaceholder={require('./images/default-banner.svg')}
  src={undefined} // undefined
  alt='Sample Banner'
/>
<div style={{ paddingTop: '5px' }}>Placeholder image is display when src props equal 'null' or 'undefined'</div>
```

Width:

```jsx
<Image width='icon-mn' ratio='1-1'
  srcPlaceholder={require('./images/default-icon.svg')}
  src={require('./images/sample-icon.svg')}
  alt='Sample Icon'
/>
<Image width='icon-tn' ratio='1-1'
  srcPlaceholder={require('./images/default-icon.svg')}
  src={require('./images/sample-icon.svg')}
  alt='Sample Icon'
/>
<Image width='icon-xxs' ratio='1-1'
  srcPlaceholder={require('./images/default-icon.svg')}
  src={require('./images/sample-icon.svg')}
  alt='Sample Icon'
/>
<Image width='icon-xs' ratio='1-1'
  srcPlaceholder={require('./images/default-icon.svg')}
  src={require('./images/sample-icon.svg')}
  alt='Sample Icon'
/>
<Image width='icon-sm' ratio='1-1'
  srcPlaceholder={require('./images/default-icon.svg')}
  src={require('./images/sample-icon.svg')}
  alt='Sample Icon'
/>
<Image width='icon-md' ratio='1-1'
  srcPlaceholder={require('./images/default-icon.svg')}
  src={require('./images/sample-icon.svg')}
  alt='Sample Icon'
/>
<Image width='icon-lg' ratio='1-1'
  srcPlaceholder={require('./images/default-icon.svg')}
  src={require('./images/sample-icon.svg')}
  alt='Sample Icon'
/>
<Image width='icon-xl' ratio='1-1'
  srcPlaceholder={require('./images/default-icon.svg')}
  src={require('./images/sample-icon.svg')}
  alt='Sample Icon'
/>
<Image width='icon-bg' ratio='1-1'
  srcPlaceholder={require('./images/default-icon.svg')}
  src={require('./images/sample-icon.svg')}
  alt='Sample Icon'
/>
<Image width='icon-hg' ratio='1-1'
  srcPlaceholder={require('./images/default-icon.svg')}
  src={require('./images/sample-icon.svg')}
  alt='Sample Icon'
/>
<Image width='icon-ms' ratio='1-1'
  srcPlaceholder={require('./images/default-icon.svg')}
  src={require('./images/sample-icon.svg')}
  alt='Sample Icon'
/>
```

Ratio:

```jsx
<div style={{ width: '375px' }}>
  <Image ratio='4-3'
    srcPlaceholder={require('./images/default-banner.svg')}
    src={require('./images/sample-banner.jpg')}
    alt='Sample Banner'
  />
</div>
<br />
<div style={{ width: '64px' }}>
  <Image ratio='1-1'
    srcPlaceholder={require('./images/default-banner.svg')}
    src={require('./images/sample-banner.jpg')}
    alt='Sample Banner'
  />
</div>
```

Curve border:

```jsx
<div style={{ width: '64px' }}>
  <Image ratio='1-1' ui='curve-border'
    srcPlaceholder={require('./images/default-banner.svg')}
    src={require('./images/sample-banner.jpg')}
    alt='Sample Banner'
  />
</div>
```
