import styled from 'styled-components'
import {
  default as VARIABLES
} from '../../themes/styles/bases/variables'
// import {
//   default as TYPOGRAPHYS
// } from '../../themes/styles/bases/typographys'
// import {
//   default as MIXINS
// } from '../../themes/styles/helpers/mixins'
// import {
//   default as UTILITIES
// } from '../../themes/styles/helpers/utilities'
// import {
//   // Example using:
//   // background: url(${CONTENTS['image-sample.svg']});
//   CONTENTS,
//   ICONS,
//   LOGOS,
//   SHAREDS,
//   DOCUMENTS
// } from '../../themes'

// Wrapper
// ============================================================
export const ImageWrapper = styled.div`
  /* Parent
  ------------------------------- */
  overflow: hidden;
  display: inline-block;
  width: 100%;
  vertical-align: middle;

  /* Childrens
  ------------------------------- */
  .image-ratio {
    position: relative;
    width: 100%;
  }

  .image-placeholder,
  .image-content {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }

  .image-placeholder {
    z-index: ${VARIABLES.Z_INDEXS.LV_1};
    /* object-fit: contain; */
    object-fit: cover;
  }

  .image-content {
    z-index: ${VARIABLES.Z_INDEXS.LV_2};
    object-fit: cover;
  }

  /* Modifiers
  ------------------------------- */
  /* UI */
  &.is-ui-curve-border {
    border: 2px solid ${VARIABLES.COLORS.GRAY_1};
    border-radius: ${VARIABLES.BORDER_RADIUSES.TN};
    background-color: ${VARIABLES.COLORS.WHITE};
  }

  /* Width */
  &.is-width-icon-mn {
    width: ${VARIABLES.ICON.SIZES.MN};
  }

  &.is-width-icon-tn {
    width: ${VARIABLES.ICON.SIZES.TN};
  }

  &.is-width-icon-xxs {
    width: ${VARIABLES.ICON.SIZES.XXS};
  }

  &.is-width-icon-xs {
    width: ${VARIABLES.ICON.SIZES.XS};
  }

  &.is-width-icon-sm {
    width: ${VARIABLES.ICON.SIZES.SM};
  }

  &.is-width-icon-md {
    width: ${VARIABLES.ICON.SIZES.MD};
  }

  &.is-width-icon-lg {
    width: ${VARIABLES.ICON.SIZES.LG};
  }

  &.is-width-icon-xl {
    width: ${VARIABLES.ICON.SIZES.XL};
  }

  &.is-width-icon-bg {
    width: ${VARIABLES.ICON.SIZES.BG};
  }

  &.is-width-icon-hg {
    width: ${VARIABLES.ICON.SIZES.HG};
  }

  &.is-width-icon-ms {
    width: ${VARIABLES.ICON.SIZES.MS};
  }

  /* Ratio */
  &.is-ratio-16-9 {
    .image-ratio {
      padding-bottom: 57%;
    }
  }

  &.is-ratio-4-3 {
    .image-ratio {
      padding-bottom: 75%;
    }
  }

  &.is-ratio-1-1 {
    .image-ratio {
      padding-bottom: 100%;
    }
  }

  /* Media queries
  ------------------------------- */
`
