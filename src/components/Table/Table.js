import React from 'react'
import ClassNames from 'classnames'
import PropTypes from 'prop-types'
import {
  TableWrapper
} from './styled'

class TableRow extends React.PureComponent {
  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([]),

    /**
    * Enable collapsible style
    */
    isCollapsible: PropTypes.bool
  }

  render() {
    const {
      className,
      children,
      ui,
      isCollapsible
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const classes = ClassNames(
      'table-row',
      { [`is-ui-${uiClasses}`]: uiClasses },
      { 'is-collapsible': isCollapsible },
      className
    )

    return (
      <div
        className={classes}
      >
        {children}
      </div>
    )
  }
}

class TableHead extends React.PureComponent {
  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([])
  }

  render () {
    const {
      className,
      children,
      ui
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const classes = ClassNames(
      'table-head',
      { [`is-ui-${uiClasses}`]: uiClasses },
      className
    )

    return (
      <div
        className={classes}
      >
        {children}
      </div>
    )
  }
}

class TableHeadColumn extends React.PureComponent {
  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([]),

    /**
    * Column width (consistent with table body column) eg. width='100px'
    */
    width: PropTypes.string,

    /**
    * Column min width for fluid column(not use 'width' props) at small screen
    */
    minWidth: PropTypes.string,

    /**
    * Change narrow width(use 'width' props) to fluid width at large screen (>= 1441px)
    */
    isFluidLargeScreen: PropTypes.bool,

    /**
    * Content horizontal/vertical alignment in column
    */
    contentAlign: PropTypes.oneOf([
      'center',
      'right',
      // Use 'isVerticalAlignTop', 'isVerticalAlignTop', 'isVerticalAlignBottom'
      // 'vertical-top',
      // 'vertical-center',
      // 'vertical-bottom'
    ]),

    /**
    * Display sortable arrow
    */
    isSortable: PropTypes.bool,

    /**
    * Display ascending/descending sortable arrow
    */
    isSortableDescending: PropTypes.bool,

    /**
    * On click event
    */
    onClick: PropTypes.func
  }

  render () {
    const {
      className,
      children,
      ui,
      width,
      minWidth,
      isFluidLargeScreen,
      contentAlign,
      isVerticalAlignTop,
      isVerticalAlignCenter,
      isVerticalAlignBottom,
      isSortable,
      isSortableDescending,
      onClick
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const contentAlignClasses = ClassNames(contentAlign)
    const classes = ClassNames(
      'table-head-column',
      { [`is-ui-${uiClasses}`]: uiClasses },
      { 'is-width-narrow': width },
      { 'is-width-fluid': isFluidLargeScreen },
      // 'contentAlign' is example props can separate to multiple props in same objective
      // can toggle multiple classes(same objective) to selected element
      { [`is-content-align-${contentAlignClasses}`]: contentAlignClasses },
      { 'is-content-align-vertical-top': isVerticalAlignTop },
      { 'is-content-align-vertical-center': isVerticalAlignCenter },
      { 'is-content-align-vertical-bottom': isVerticalAlignBottom },
      { 'is-onclick': onClick },
      { 'is-sortable': isSortable },
      className
    )

    return (
      <div
        className={classes}
        style={{
          maxWidth: width,
          minWidth: minWidth
        }}
        onClick={onClick}
      >
        {children}
        {
          isSortable &&
            <React.Fragment>
              {
                isSortableDescending ?
                  <div className='table-head-column-sortable-arrow is-descending' />
                  :
                  <div className='table-head-column-sortable-arrow is-ascending' />
              }
            </React.Fragment>
        }
      </div>
    )
  }
}

class TableHeadText extends React.PureComponent {
  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([])
  }

  render() {
    const {
      className,
      children,
      ui
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const classes = ClassNames(
      'table-head-text',
      { [`is-ui-${uiClasses}`]: uiClasses },
      className
    )

    return (
      <div
        className={classes}
      >
        {children}
      </div>
    )
  }
}

class TableBody extends React.PureComponent {
  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([])
  }

  render () {
    const {
      className,
      children,
      ui
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const classes = ClassNames(
      'table-body',
      { [`is-ui-${uiClasses}`]: uiClasses },
      className
    )

    return (
      <div
        className={classes}
      >
        {children}
      </div>
    )
  }
}

class TableBodyColumn extends React.PureComponent {
  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([]),

    /**
    * Column width (consistent with table body column) eg. width='100px'
    */
    width: PropTypes.string,

    /**
    * Column min width for fluid column(not use 'width' props) at small screen
    */
    minWidth: PropTypes.string,

    /**
    * Change narrow width(use 'width' props) to fluid width at large screen (>= 1441px)
    */
    isFluidLargeScreen: PropTypes.bool,

    /**
    * Content horizontal/vertical alignment in column
    */
    contentAlign: PropTypes.oneOf([
      'center',
      'right',
      // Use 'isVerticalAlignTop', 'isVerticalAlignTop', 'isVerticalAlignBottom'
      // 'vertical-top',
      // 'vertical-center',
      // 'vertical-bottom'
    ]),

    /**
    * Display sortable arrow
    */
    isSortable: PropTypes.bool,

    /**
    * Display ascending/descending sortable arrow
    */
    isSortableDescending: PropTypes.bool,

    /**
    * On click event
    */
    onClick: PropTypes.func
  }

  render() {
    const {
      className,
      children,
      ui,
      width,
      minWidth,
      isFluidLargeScreen,
      contentAlign,
      isVerticalAlignTop,
      isVerticalAlignCenter,
      isVerticalAlignBottom,
      isCollapsibleArrow,
      isCollapsibleArrowOpen,
      onClick
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const contentAlignClasses = ClassNames(contentAlign)
    const classes = ClassNames(
      'table-body-column',
      { [`is-ui-${uiClasses}`]: uiClasses },
      { 'is-width-narrow': width },
      { 'is-width-fluid': isFluidLargeScreen },
      { [`is-content-align-${contentAlignClasses}`]: contentAlignClasses },
      { 'is-content-align-vertical-top': isVerticalAlignTop },
      { 'is-content-align-vertical-center': isVerticalAlignCenter },
      { 'is-content-align-vertical-bottom': isVerticalAlignBottom },
      { 'is-onclick': onClick },
      { 'is-collapsible-arrow': isCollapsibleArrow },
      className
    )

    return (
      <div
        className={classes}
        style={{
          maxWidth: width,
          minWidth: minWidth
        }}
        onClick={onClick}
      >
        {children}

        {
          isCollapsibleArrow &&
            <React.Fragment>
              {
                isCollapsibleArrowOpen ?
                  <div className='table-body-column-collapsible-arrow is-open' />
                  :
                  <div className='table-body-column-collapsible-arrow is-close' />
              }
            </React.Fragment>
        }
      </div>
    )
  }
}

class TableBodyText extends React.PureComponent {
  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([]),

    /**
    * Display ellipsis (single line with '...' when content over width than column width)
    */
    isEllipsis: PropTypes.bool
  }

  render() {
    const {
      className,
      children,
      ui,
      isEllipsis
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const classes = ClassNames(
      'table-body-text',
      { [`is-ui-${uiClasses}`]: uiClasses },
      { 'is-ellipsis': isEllipsis },
      className
    )

    return (
      <div
        className={classes}
        title={isEllipsis ? `${children}` : null}
      >
        {children}
      </div>
    )
  }
}

class TableGroup extends React.PureComponent {
  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([])
  }

  render() {
    const {
      className,
      children,
      ui
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const classes = ClassNames(
      'table-group',
      { [`is-ui-${uiClasses}`]: uiClasses },
      className
    )

    return (
      <div
        className={classes}
      >
        {children}
      </div>
    )
  }
}

class TableCollapsible extends React.PureComponent {
  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([]),

    /**
    * Display content
    */
    isOpen: PropTypes.bool
  }

  render() {
    const {
      className,
      children,
      ui,
      isOpen
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const classes = ClassNames(
      'table-collapsible',
      { [`is-ui-${uiClasses}`]: uiClasses },
      { 'is-open': isOpen },
      className
    )

    return (
      <div
        className={classes}
      >
        {children}
      </div>
    )
  }
}

/**
 * Table description:
 * - row and column layout for display more info data like excel
 */

export class Table extends React.PureComponent {
  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([])
  }

  static Row = TableRow
  static Head = TableHead
  static HeadColumn = TableHeadColumn
  static HeadText = TableHeadText
  static Body = TableBody
  static BodyColumn = TableBodyColumn
  static BodyText = TableBodyText
  static Group = TableGroup
  static Collapsible = TableCollapsible

  render () {
    const {
      className,
      children,
      ui
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const classes = ClassNames(
      'table',
      { [`is-ui-${uiClasses}`]: uiClasses },
      className
    )

    return (
      <TableWrapper
        className={classes}
      >
        <div className='table-scroll'>
          {children}
        </div>
      </TableWrapper>
    )
  }
}
