Default:

```jsx
<Table>
  <Table.Head>
    <Table.Row>
      <Table.HeadColumn>
        <Table.HeadText>TH_1</Table.HeadText>
      </Table.HeadColumn>
      <Table.HeadColumn>
        <Table.HeadText>TH_2</Table.HeadText>
      </Table.HeadColumn>
      <Table.HeadColumn>
        <Table.HeadText>TH_3</Table.HeadText>
      </Table.HeadColumn>
    </Table.Row>
  </Table.Head>
  <Table.Body>
    <Table.Row>
      <Table.BodyColumn>
        <Table.BodyText>TD_1</Table.BodyText>
      </Table.BodyColumn>
      <Table.BodyColumn>
        <Table.BodyText>TD_2</Table.BodyText>
      </Table.BodyColumn>
      <Table.BodyColumn>
        <Table.BodyText>TD_3</Table.BodyText>
      </Table.BodyColumn>
    </Table.Row>
  </Table.Body>
</Table>
```

Width:

```jsx
<Table>
  <Table.Head>
    <Table.Row>
      <Table.HeadColumn width='150px'>
        <Table.HeadText>Width_150px</Table.HeadText>
      </Table.HeadColumn>
      <Table.HeadColumn width='350px' isFluidLargeScreen>
        <Table.HeadText>Width_350px_FluidLargeScreen</Table.HeadText>
      </Table.HeadColumn>
      <Table.HeadColumn minWidth='200px'>
        <Table.HeadText>MinWidth_200px</Table.HeadText>
      </Table.HeadColumn>
    </Table.Row>
  </Table.Head>
  <Table.Body>
    <Table.Row>
      <Table.BodyColumn width='150px'>
        <Table.BodyText>TD_1</Table.BodyText>
      </Table.BodyColumn>
      <Table.BodyColumn width='350px' isFluidLargeScreen>
        <Table.BodyText>TD_2</Table.BodyText>
      </Table.BodyColumn>
      <Table.BodyColumn minWidth='200px'>
        <Table.BodyText>TD_3</Table.BodyText>
      </Table.BodyColumn>
    </Table.Row>
  </Table.Body>
</Table>
<div style={{
  padding: '15px',
  backgroundColor: '#EEEEEE'
}}>
  Note:
  <br />
  - HeadColumn and BodyColumn must has a same 'width' or 'minWidth' or 'isFluidLargeScreen' props
  <br />
  - 'isFluidLargeScreen' props change narrow width(use 'width' props) to fluid width at large screen (>= 1441px)
  <br />
  - 'minWidth' for fluid column(not use 'width' props) at small screen
</div>
```

Scrollbar:

```jsx
<Table>
  <Table.Head>
    <Table.Row>
      <Table.HeadColumn width='150px'>
        <Table.HeadText>TH_1</Table.HeadText>
      </Table.HeadColumn>
      <Table.HeadColumn width='750px'>
        <Table.HeadText>TH_2</Table.HeadText>
      </Table.HeadColumn>
      <Table.HeadColumn minWidth='200px'>
        <Table.HeadText>TH_3</Table.HeadText>
      </Table.HeadColumn>
    </Table.Row>
  </Table.Head>
  <Table.Body>
    <Table.Row>
      <Table.BodyColumn width='150px'>
        <Table.BodyText>TD_1</Table.BodyText>
      </Table.BodyColumn>
      <Table.BodyColumn width='750px' isFluidLargeScreen>
        <Table.BodyText>TD_2</Table.BodyText>
      </Table.BodyColumn>
      <Table.BodyColumn minWidth='200px'>
        <Table.BodyText>TD_3</Table.BodyText>
      </Table.BodyColumn>
    </Table.Row>
  </Table.Body>
</Table>
<div style={{
  padding: '15px',
  marginTop: '15px',
  backgroundColor: '#EEEEEE'
}}>
  Note: scrollbar will displayed when container width less than table width
</div>
```

Content horizontal alignment:

```jsx
<Table>
  <Table.Head>
    <Table.Row>
      <Table.HeadColumn>
        <Table.HeadText>TH_1</Table.HeadText>
      </Table.HeadColumn>
      <Table.HeadColumn contentAlign='center'>
        <Table.HeadText>TH_2</Table.HeadText>
      </Table.HeadColumn>
      <Table.HeadColumn contentAlign='right'>
        <Table.HeadText>TH_3</Table.HeadText>
      </Table.HeadColumn>
    </Table.Row>
  </Table.Head>
  <Table.Body>
    <Table.Row>
      <Table.BodyColumn>
        <Table.BodyText>TD_1</Table.BodyText>
      </Table.BodyColumn>
      <Table.BodyColumn contentAlign='center'>
        <Table.BodyText>TD_2</Table.BodyText>
      </Table.BodyColumn>
      <Table.BodyColumn contentAlign='right'>
        <Table.BodyText>TD_3</Table.BodyText>
      </Table.BodyColumn>
    </Table.Row>
  </Table.Body>
</Table>
```

Content vertical alignment:

```jsx
<Table>
  <Table.Head>
    <Table.Row>
      <Table.HeadColumn width='200px'>
        <Table.HeadText>TH_1</Table.HeadText>
      </Table.HeadColumn>
      <Table.HeadColumn>
        <Table.HeadText>TH_2</Table.HeadText>
      </Table.HeadColumn>
      <Table.HeadColumn>
        <Table.HeadText>TH_3</Table.HeadText>
      </Table.HeadColumn>
    </Table.Row>
  </Table.Head>
  <Table.Body>
    <Table.Row>
      <Table.BodyColumn width='200px'>
        <Table.BodyText>
          Lorem ipsum dolor sit, amet consectetur adipisicing elit. Aspernatur quae recusandae reprehenderit tempore. Voluptate enim facere voluptatem ea dolores fuga. Nesciunt unde minima consectetur earum. Ipsam magnam at necessitatibus saepe.
        </Table.BodyText>
      </Table.BodyColumn>
      <Table.BodyColumn isVerticalAlignCenter>
        <Table.BodyText>TD_2</Table.BodyText>
      </Table.BodyColumn>
      <Table.BodyColumn isVerticalAlignBottom>
        <Table.BodyText>TD_3</Table.BodyText>
      </Table.BodyColumn>
    </Table.Row>
  </Table.Body>
</Table>
```

Ellipsis with hover title:

```jsx
<Table>
  <Table.Head>
    <Table.Row>
      <Table.HeadColumn width='200px'>
        <Table.HeadText>TH_1</Table.HeadText>
      </Table.HeadColumn>
      <Table.HeadColumn>
        <Table.HeadText>TH_2</Table.HeadText>
      </Table.HeadColumn>
      <Table.HeadColumn>
        <Table.HeadText>TH_3</Table.HeadText>
      </Table.HeadColumn>
    </Table.Row>
  </Table.Head>
  <Table.Body>
    <Table.Row>
      <Table.BodyColumn width='200px'>
        <Table.BodyText isEllipsis>
          Lorem ipsum dolor sit, amet consectetur adipisicing elit. Aspernatur quae recusandae reprehenderit tempore. Voluptate enim facere voluptatem ea dolores fuga. Nesciunt unde minima consectetur earum. Ipsam magnam at necessitatibus saepe.
        </Table.BodyText>
      </Table.BodyColumn>
      <Table.BodyColumn isVerticalAlignCenter>
        <Table.BodyText>TD_2</Table.BodyText>
      </Table.BodyColumn>
      <Table.BodyColumn isVerticalAlignBottom>
        <Table.BodyText>TD_3</Table.BodyText>
      </Table.BodyColumn>
    </Table.Row>
  </Table.Body>
</Table>
```

Collapsible arrow button and content:

```jsx
<Table>
  <Table.Head>
    <Table.Row>
      <Table.HeadColumn>
        <Table.HeadText>TH_1</Table.HeadText>
      </Table.HeadColumn>
      <Table.HeadColumn>
        <Table.HeadText>TH_2</Table.HeadText>
      </Table.HeadColumn>
      <Table.HeadColumn>
        <Table.HeadText>TH_3</Table.HeadText>
      </Table.HeadColumn>
    </Table.Row>
  </Table.Head>
  <Table.Body>
    <Table.Row
      isCollapsible
    >
      <Table.Group>
        <Table.BodyColumn
          isCollapsibleArrow
          isCollapsibleArrowOpen={true}
          onClick={() => { alert('Open collapsible') }}
        >
          <Table.BodyText>TD_1</Table.BodyText>
        </Table.BodyColumn>
        <Table.BodyColumn>
          <Table.BodyText>TD_2</Table.BodyText>
        </Table.BodyColumn>
        <Table.BodyColumn>
          <Table.BodyText>TD_3</Table.BodyText>
        </Table.BodyColumn>
      </Table.Group>
      <Table.Collapsible
        isOpen={true}
      >
        <Table.BodyColumn>
          <Table.BodyText>Collapsible content...</Table.BodyText>
        </Table.BodyColumn>
      </Table.Collapsible>
    </Table.Row>
    <Table.Row
      isCollapsible
    >
      <Table.Group>
        <Table.BodyColumn
          isCollapsibleArrow
          isCollapsibleArrowOpen={false}
          onClick={() => { alert('Open collapsible') }}
        >
          <Table.BodyText>TD_1</Table.BodyText>
        </Table.BodyColumn>
        <Table.BodyColumn>
          <Table.BodyText>TD_2</Table.BodyText>
        </Table.BodyColumn>
        <Table.BodyColumn>
          <Table.BodyText>TD_3</Table.BodyText>
        </Table.BodyColumn>
      </Table.Group>
      <Table.Collapsible
        isOpen={false}
      >
        <Table.BodyColumn>
          <Table.BodyText>Collapsible content...</Table.BodyText>
        </Table.BodyColumn>
      </Table.Collapsible>
    </Table.Row>
  </Table.Body>
</Table>
```

Sortable icon:

```jsx
<Table>
  <Table.Head>
    <Table.Row>
      <Table.HeadColumn
        isSortable
        isSortableDescending={true}
        onClick={() => { alert('Sorting!!!') }}
      >
        <Table.HeadText>TH_1</Table.HeadText>
      </Table.HeadColumn>
      <Table.HeadColumn
        isSortable
        isSortableDescending={false}
        onClick={() => { alert('Sorting!!!') }}
      >
        <Table.HeadText>TH_2</Table.HeadText>
      </Table.HeadColumn>
      <Table.HeadColumn
        isSortable
        isSortableDescending={false}
        onClick={() => { alert('Sorting!!!') }}
      >
        <Table.HeadText>TH_3</Table.HeadText>
      </Table.HeadColumn>
    </Table.Row>
  </Table.Head>
  <Table.Body>
    <Table.Row>
      <Table.BodyColumn>
        <Table.BodyText>TD_1</Table.BodyText>
      </Table.BodyColumn>
      <Table.BodyColumn>
        <Table.BodyText>TD_2</Table.BodyText>
      </Table.BodyColumn>
      <Table.BodyColumn>
        <Table.BodyText>TD_3</Table.BodyText>
      </Table.BodyColumn>
    </Table.Row>
  </Table.Body>
</Table>
```
