import styled from 'styled-components'
import {
  default as VARIABLES
} from '../../themes/styles/bases/variables'
import {
  default as TYPOGRAPHYS
} from '../../themes/styles/bases/typographys'
import {
  default as MIXINS
} from '../../themes/styles/helpers/mixins'
// import {
//   default as UTILITIES
// } from '../../themes/styles/helpers/utilities'
// import {
//   // Example using:
//   // background: url(${CONTENTS['image-Field.svg']});
//   CONTENTS,
//   ICONS,
//   LOGOS,
//   SHAREDS,
//   DOCUMENTS
// } from '../../themes'

// Wrapper
// ============================================================
export const FieldWrapper = styled.div`
  /* Parent
  ------------------------------- */
  /* Childrens
  ------------------------------- */
  /* Input (text/password) */
  .field-input {
    display: inline-block;
    vertical-align: middle;

    /* Label width */
    &.is-label-width {
      &-sm {
        .field-input-label {
          width: ${VARIABLES.FIELD.LABEL_WIDTHS.SM};
        }
      }

      &-md {
        .field-input-label {
          width: ${VARIABLES.FIELD.LABEL_WIDTHS.MD};
        }
      }

      &-lg {
        .field-input-label {
          width: ${VARIABLES.FIELD.LABEL_WIDTHS.LG};
        }
      }
    }

    /* Data width */
    &.is-data-width {
      &-sm {
        .field-input-data,
        .field-input-data-area,
        .field-input-data-select {
          width: ${VARIABLES.FIELD.WIDTHS.SM};
        }
      }

      &-md {
        .field-input-data,
        .field-input-data-area,
        .field-input-data-select {
          width: ${VARIABLES.FIELD.WIDTHS.MD};
        }
      }

      &-lg {
        .field-input-data,
        .field-input-data-area,
        .field-input-data-select {
          width: ${VARIABLES.FIELD.WIDTHS.LG};
        }
      }
    }
  }

  .field-input-container {
    display: flex;
  }

  .field-input-label {
    ${TYPOGRAPHYS.FONT_STYLES.FIRST_BOLD_TN};
    padding-top: 7px;
    margin-right: 10px;
    color: ${VARIABLES.COLORS.TEXT_HEAD};
  }

  .field-input-group {
    position: relative;
  }

  .field-input-message,
  .field-input-hint {
    ${TYPOGRAPHYS.FONT_STYLES.FIRST_REGULAR_MN};
    color: ${VARIABLES.COLORS.TEXT_DETAIL};
  }

  .field-input-message {
    display: none;
    margin-top: 5px;
  }

  .field-input-hint {
    padding-top: 8px;
    margin-left: 10px;
  }

  .field-input-data,
  .field-input-data-area,
  .field-input-data-select {
    ${MIXINS.PLACEHOLDER({})};
    ${TYPOGRAPHYS.FONT_STYLES.FIRST_REGULAR_TN};
    width: ${VARIABLES.FIELD.WIDTHS.XS};
    padding-left: 10px;
    background-color: ${VARIABLES.COLORS.WHITE};
    border: 1px solid ${VARIABLES.COLORS.TEXT_DETAIL};
    border-radius: ${VARIABLES.FIELD.BORDER_RADIUS};
    color: ${VARIABLES.COLORS.TEXT_HEAD};

    &:focus {
      outline: none;
    }
  }

  .field-input-data,
  .field-input-data-select {
    height: ${VARIABLES.FIELD.TYPES.TEXT.HEIGHT};
  }

  .field-input-data,
  .field-input-data-area {
    padding-right: 10px;
  }

  .field-input-data-area {
    resize: none;
    padding-top: 5px;
    padding-bottom: 5px;
    height: ${VARIABLES.FIELD.TYPES.TEXTAREA.HEIGHT};
  }

  /* Select */
  .field-input-data-select {
    padding-right: ${VARIABLES.FIELD.TYPES.TEXT.HEIGHT};
  }

  .field-input-data-select-arrow {
    pointer-events: none;
    position: absolute;
    z-index: ${VARIABLES.Z_INDEXS.LV_1};
    top: 1px;
    right: 1px;
    width: ${VARIABLES.FIELD.TYPES.TEXT.HEIGHT};
    height: calc(${VARIABLES.FIELD.TYPES.TEXT.HEIGHT}  - 2px);
    background-color: ${VARIABLES.COLORS.WHITE};
    border-top-right-radius: ${VARIABLES.FIELD.BORDER_RADIUS};
    border-bottom-right-radius: ${VARIABLES.FIELD.BORDER_RADIUS};

    &:after {
      transform: translate(-50%, -50%);
      content: ' ';
      position: absolute;
      top: 50%;
      left: 50%;
      width: 0;
      height: 0;
      margin-top: 2px;
      border: solid transparent;
      border-color: rgba(255, 255, 255, 0);
      border-top-color: ${VARIABLES.COLORS.TEXT_HEAD};
      border-width: 4px;
    }
  }

  /* File */
  .field-input-data-file {
    display: flex;
    align-items: center;

    * {
      margin-right: 10px;

      &:last-child {
        margin-right: 0;
      }
    }

    input[type=file] {
      opacity: 0;
      cursor: pointer;
      position: absolute;
      z-index: ${VARIABLES.Z_INDEXS.LV_1};
      top: 0;
      left: 0;
      font-size: 100px;

      &:focus {
        outline: none;
      }

      &::-webkit-file-upload-button {
        cursor: pointer;
      }
    }

    /* After browse file */
    &.is-browsed {
      .field-input-data-file-button {
        &.is-upload {
          pointer-events: auto;
          background-color: ${VARIABLES.COLORS.GREEN_1};
        }
      }
    }
  }

  .field-input-data-file-button {
    transition: ${VARIABLES.TRANSITIONS.DEFAULT};
    cursor: pointer;
    overflow: hidden;
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    width: ${VARIABLES.BUTTON.WIDTHS.SM};
    height: ${VARIABLES.BUTTON.HEIGHTS.SM};
    background-color: ${VARIABLES.COLORS.BLUE_1};
    border-radius: ${VARIABLES.BORDER_RADIUSES.XXS};

    &.is-upload {
      pointer-events: none;
      background-color: ${VARIABLES.COLORS.GRAY_1};
    }
  }

  .field-input-data-file-button-text {
    ${TYPOGRAPHYS.FONT_STYLES.FIRST_REGULAR_XXS};
    color: ${VARIABLES.COLORS.WHITE};
  }

  .field-input-data-file-name {
    display: flex;
    align-items: center;
    width: ${VARIABLES.FIELD.WIDTHS.XS};
    height: ${VARIABLES.BUTTON.HEIGHTS.SM};
    padding-right: 10px;
    padding-left: 10px;
    background-color: ${VARIABLES.COLORS.WHITE};
    border: 1px solid ${VARIABLES.COLORS.TEXT_DETAIL};
    border-radius: ${VARIABLES.FIELD.BORDER_RADIUS};
  }

  .field-input-data-file-name-text {
    ${MIXINS.ELLIPSIS({})};
    ${TYPOGRAPHYS.FONT_STYLES.FIRST_REGULAR_TN};
    color: ${VARIABLES.COLORS.TEXT_HEAD};
  }

  /* Checkbox, Radio and Enable */
  .field-toggle {
    cursor: pointer;
    position: relative;
    display: inline-block;
    vertical-align: middle;

    + .field-input {
      margin-left: 10px;
    }

    /* UI - Radio */
    &.is-ui-radio {
      .field-toggle-label-icon {
        &:before {
          border-radius: 50%;
        }

        &:after {
          transform: translate(-50%, -50%);
          top: 50%;
          left: 50%;
          width: calc(${VARIABLES.CHECKBOX.WIDTH} - 6px);
          height: calc(${VARIABLES.CHECKBOX.HEIGHT} - 6px);
          background-image: none;
          background-color: ${VARIABLES.COLORS.BLACK};
          border-radius: 50%;
        }
      }
    }

    /* UI - Switch */
    &.is-ui-switch {
      .field-toggle-label-icon {
        display: flex;
        align-items: center;
        min-width: 45px;
        height: 22px;
        margin-right: 0;
        padding-right: 10px;
        padding-left: 25px;
        background-color: ${VARIABLES.COLORS.GRAY_1};
        border-color: ${VARIABLES.COLORS.TEXT_DETAIL};
        border-radius: 20px;

        &:before {
          display: none;
        }

        &:after {
          visibility: visible;
          opacity: 1;
          top: 3px;
          left: 3px;
          width: 16px;
          height: 16px;
          background: ${VARIABLES.COLORS.WHITE};
          border-radius: 50%;
          box-shadow: 0 0 5px rgba(0, 0, 0, 0.25);
        }
      }

      .field-toggle-label-text {
        display: none;
      }

      /* Checked */
      .field-toggle-input {
        &:checked {
          + .field-toggle-label {
            .field-toggle-label-icon {
              padding-right: 25px;
              padding-left: 10px;
              background-color: ${VARIABLES.COLORS.SYSTEM_SUCCESS};

              &:after {
                left: auto;
                right: 3px;
              }
            }

            .field-toggle-label-text-switch {
              color: ${VARIABLES.COLORS.WHITE};
            }
          }
        }
      }
    }
  }

  .field-toggle-input {
    cursor: pointer;
    opacity: 0;
    position: absolute;
    z-index: ${VARIABLES.Z_INDEXS.LV_3};
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;

    &:checked {
      + .field-toggle-label {
        .field-toggle-label-icon {
          &:after {
            visibility: visible;
            opacity: 1;
          }
        }
      }
    }
  }

  .field-toggle-label {
    display: flex;
    align-items: center;
  }

  .field-toggle-label-icon {
    position: relative;
    margin-right: 10px;

    &:before,
    &:after {
      transition: ${VARIABLES.TRANSITIONS.DEFAULT};
      content: ' ';
      display: block;
    }

    &:before {
      width: ${VARIABLES.CHECKBOX.WIDTH};
      height: ${VARIABLES.CHECKBOX.HEIGHT};
      border: 1px solid ${VARIABLES.COLORS.BLACK};
      border-radius: ${VARIABLES.CHECKBOX.BORDER_RADIUS};
    }

    &:after {
      visibility: hidden;
      opacity: 0;
      position :absolute;
      z-index: ${VARIABLES.Z_INDEXS.LV_2};
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background: url(${require('./images/icon-done-black.svg')}) no-repeat center;
      background-size: ${VARIABLES.CHECKBOX.ICON.WIDTH} ${VARIABLES.CHECKBOX.ICON.HEIGHT};
    }
  }

  .field-toggle-label-text {
    ${TYPOGRAPHYS.FONT_STYLES.FIRST_REGULAR_TN};
    color: ${VARIABLES.COLORS.TEXT_HEAD};
  }

  .field-toggle-label-text-switch {
    ${TYPOGRAPHYS.FONT_STYLES.FIRST_REGULAR_MN};
    color: ${VARIABLES.COLORS.TEXT_HEAD};
  }

  /* Text info */
  .field-input-text-info {
    ${TYPOGRAPHYS.FONT_STYLES.FIRST_REGULAR_TN};
    padding-top: 6px;
    color: ${VARIABLES.COLORS.TEXT_HEAD};
  }

  /* Modifiers
  ------------------------------- */
  /* UI */
  &.is-ui-disabled {
    .field-input {
      .field-input-data,
      .field-input-data-area,
      .field-input-data-select,
      .field-input-data-select-arrow,
      .field-input-data-file-name,
      .field-input-data-file-button {
        pointer-events: none;
        background-color: ${VARIABLES.COLORS.GRAY_1};
      }
    }

    .field-toggle {
      pointer-events: none;

      .field-toggle-label-icon {
        &:before {
          background-color: ${VARIABLES.COLORS.GRAY_1};
        }
      }

      &.is-ui-switch {
        .field-toggle-label-icon {
          &:after {
            background-color: ${VARIABLES.COLORS.GRAY_1};
          }
        }
      }
    }
  }

  /* UI - Validation */
  &.is-ui-validation-error {
    .field-input-data,
    .field-input-data-area,
    .field-input-data-select,
    .field-input-data-file-name {
      border-color: ${VARIABLES.COLORS.SYSTEM_ERROR};
    }

    .field-input-message {
      display: block;
      color: ${VARIABLES.COLORS.SYSTEM_ERROR};
    }
  }

  /* Display */
  &.is-display-message {
    .field-input-message {
      display: block;
    }
  }

  /* Media queries
  ------------------------------- */
`
