import styled from 'styled-components'
// import {
//   default as VARIABLES
// } from '../../themes/styles/bases/variables'
// import {
//   default as TYPOGRAPHYS
// } from '../../themes/styles/bases/typographys'
// import {
//   default as MIXINS
// } from '../../themes/styles/helpers/mixins'
// import {
//   default as UTILITIES
// } from '../../themes/styles/helpers/utilities'
// import {
//   // Example using:
//   // background: url(${CONTENTS['image-TextEditor.svg']});
//   CONTENTS,
//   ICONS,
//   LOGOS,
//   SHAREDS,
//   DOCUMENTS
// } from '../../themes'

// Wrapper
// ============================================================
export const TextEditorWrapper = styled.div`
  /* Parent
  ------------------------------- */
  /* Childrens
  ------------------------------- */
  /* Modifiers
  ------------------------------- */
  &.is-ui-react-quill {
    .ql-picker-label {
      &:focus {
        outline: none;
      }
    }

    .ql-editor {
      min-height: 250px;

      /* Default properties */
      * {}

      /* Heading */
      h1 {}

      h2 {}

      h3 {}

      h4 {}

      h5 {}

      h6 {}

      /* Normal */
      p {}

      /* Text transform */
      strong,
      em,
      u,
      s {
        vertical-align: baseline;
      }

      /* Text sub & sup */
      sub {}

      sup {}

      /* Text color & background color */
      p {
        span {
          &[style*='color'] {}

          &[style*='background-color'] {}
        }
      }

      /* Text align */
      .ql-align-center {}

      .ql-align-right {}

      .ql-align-justify {}

      /* Order & Bullet list */
      ol,
      ul {
        padding-left: 0;

        li {
          &:not(.ql-direction-rtl),
          &:not(.ql-direction-rtl) {
            padding-left: 15px;
          }
        }
      }

      /* Text indent */
      .ql-indent-1 {
        padding-left: 15px;
      }

      .ql-indent-2 {
        padding-left: calc(15px * 2);
      }

      .ql-indent-3 {
        padding-left: calc(15px * 3);
      }

      .ql-indent-4 {
        padding-left: calc(15px * 4);
      }

      .ql-indent-5 {
        padding-left: calc(15px * 5);
      }

      .ql-indent-6 {
        padding-left: calc(16px * 5);
      }

      /* Link */
      a {}

      /* Image */
      img {}

      /* Video link(iframe) */
      .ql-video {}
    }
  }

  /* Media queries
  ------------------------------- */
`
