import React from 'react'
import ClassNames from 'classnames'
import PropTypes from 'prop-types'
import {
  TextEditorWrapper
} from './styled'

/**
 * TextEditor description:
 * - Wrapper/Container for add parent class and then overwrite default module style
 * - react-quill, https://github.com/zenoamaro/react-quill and https://quilljs.com/
 */

export class TextEditor extends React.PureComponent {
  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    ui: PropTypes.oneOf([
      'react-quill'
    ])
  }

  render () {
    const {
      className,
      children,
      ui
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const classes = ClassNames(
      'texteditor',
      { [`is-ui-${uiClasses}`]: uiClasses },
      className
    )

    return (
      <TextEditorWrapper
        className={classes}
      >
        {children}
      </TextEditorWrapper>
    )
  }
}
