Default:

```jsx
  <Button>Default</Button>
```

Width/Height/Color/Font size & color:

```jsx
  <Button width='xs' height='xs' bgColor='red-1' fontSize='mn' textColor='black'>Extra small</Button>
  <br />
  <Button width='sm' height='sm' bgColor='blue-1' fontSize='xxs'>Small</Button>
  <br />
  <Button width='md' height='md' bgColor='green-1' fontSize='sm'>Medium</Button>
  <br />
  <Button width='lg' height='lg' fontSize='lg'>Large</Button>
  <br />
  <Button width='xl' height='xl' fontSize='bg'>Extra large</Button>
```

Border width/radius:

```jsx
  <Button borderWidth='xxs' borderColor='red-1' borderRadius='xxs'>Default</Button>
```

State:

```jsx
  <Button ui='disabled'>Default</Button>
```

Icon:

```jsx
  <Button width='sm' height='xs'
    frontIcon={
      <img src={require('./images/sample-icon.svg')} style={{ display: 'block', width: '14px', height: '14px' }} alt='Icon' />
    }
  >
    Extra small
  </Button>
  <br />
  <Button width='sm' height='xs'
    backIcon={
      <img src={require('./images/sample-icon.svg')} style={{ display: 'block', width: '14px', height: '14px' }} alt='Icon' />
    }
  >
    Extra small
  </Button>
```
