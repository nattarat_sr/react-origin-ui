import React from 'react'
import ClassNames from 'classnames'
import PropTypes from 'prop-types'
import {
  ButtonWrapper
} from './styled'

/**
 * Button description:
 * - Variety of button
 */

export class Button extends React.PureComponent {
  static defaultProps = {
    width: 'sm',
    height: 'sm',
    fontSize: 'xxs',
    textColor: 'white',
    bgColor: 'black',
    borderRadius: 'xxs'
  }

  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    ui: PropTypes.oneOf([
      'border', 'disabled'
    ]),

    /**
    * Width:
    * xs (100px), sm (125px), md (150px), lg (200px), xl (250px)
    */
    width: PropTypes.oneOf([
      'xs', 'sm', 'md', 'lg', 'xl'
    ]),

    /**
    * Height:
    * xs (24px), sm (32px), md (48px), lg (60px), xl (72px)
    */
    height: PropTypes.oneOf([
      'xs', 'sm', 'md', 'lg', 'xl'
    ]),

    /**
    * Background color
    */
    bgColor: PropTypes.oneOf([
      'black', 'white', 'red-1', 'green-1', 'blue-1',
      'text-head', 'text-sub-head', 'text-detail', 'text-link', 'call-to-action',
      'validation-error', 'validation-success'
    ]),

    /**
    * Border width
    */
    borderWidth: PropTypes.oneOf([
      'mn', 'tn', 'xxs', 'xs', 'sm', 'md', 'lg', 'xl', 'bg', 'hg', 'ms'
    ]),

    /**
    * Border color
    */
    borderColor: PropTypes.oneOf([
      'black', 'white', 'red-1', 'green-1', 'blue-1',
      'text-head', 'text-sub-head', 'text-detail', 'text-link', 'call-to-action',
      'validation-error', 'validation-success'
    ]),

    /**
    * Border radius
    */
    borderRadius: PropTypes.oneOf([
      'mn', 'tn', 'xxs', 'xs', 'sm', 'md', 'lg', 'xl', 'bg', 'hg', 'ms'
    ]),

    /**
    * Font size
    */
    fontSize: PropTypes.oneOf([
      'mn', 'tn', 'xxs', 'xs', 'sm', 'md', 'lg', 'xl', 'bg', 'hg', 'ms'
    ]),

    /**
    * Text color
    */
    textColor: PropTypes.oneOf([
      'black', 'white', 'red-1', 'green-1', 'blue-1',
      'text-head', 'text-sub-head', 'text-detail', 'text-link', 'call-to-action',
      'validation-error', 'validation-success'
    ]),

    /**
    * Front icon
    */
    frontIcon: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Back icon
    */
    backIcon: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * On click event
    */
    onClick: PropTypes.func
  }

  // static Text = ButtonText

  render () {
    const {
      className,
      children,
      ui,
      width,
      height,
      bgColor,
      borderWidth,
      borderColor,
      borderRadius,
      fontSize,
      textColor,
      frontIcon,
      backIcon,
      onClick
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const widthClasses = ClassNames(width)
    const heightClasses = ClassNames(height)
    const bgColorClasses = ClassNames(bgColor)
    const borderWidthClasses = ClassNames(borderWidth)
    const borderColorClasses = ClassNames(borderColor)
    const borderRadiusClasses = ClassNames(borderRadius)
    const classes = ClassNames(
      'button',
      { [`is-ui-${uiClasses}`]: uiClasses },
      { [`is-ui-border`]: borderWidthClasses },
      { [`is-width-${widthClasses}`]: widthClasses },
      { [`is-height-${heightClasses}`]: heightClasses },
      { [`is-bgcolor-${bgColorClasses}`]: bgColorClasses },
      { [`is-borderwidth-${borderWidthClasses}`]: borderWidthClasses },
      { [`is-bordercolor-${borderColorClasses}`]: borderColorClasses },
      { [`is-borderradius-${borderRadiusClasses}`]: borderRadiusClasses },
      className
    )

    const fontSizeClasses = ClassNames(fontSize)
    const textColorClasses = ClassNames(textColor)
    const textClasses = ClassNames(
      'button-text',
      { [`is-fontsize-${fontSizeClasses}`]: fontSizeClasses },
      { [`is-textcolor-${textColorClasses}`]: textColorClasses }
    )

    return (
      <ButtonWrapper
        className={classes}
        onClick={onClick}
      >
        {
          frontIcon &&
            <div className='button-icon is-front'>
              {frontIcon}
            </div>
        }
        <div className={textClasses}>
          {children}
        </div>
        {
          backIcon &&
            <div className='button-icon is-back'>
              {backIcon}
            </div>
        }
      </ButtonWrapper>
    )
  }
}
