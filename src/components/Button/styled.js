import styled from 'styled-components'
import {
  default as VARIABLES
} from '../../themes/styles/bases/variables'
// import {
//   default as TYPOGRAPHYS
// } from '../../themes/styles/bases/typographys'
// import {
//   default as MIXINS
// } from '../../themes/styles/helpers/mixins'
import {
  default as UTILITIES
} from '../../themes/styles/helpers/utilities'
// import {
//   // Example using:
//   // background: url(${CONTENTS['image-sample.svg']});
//   CONTENTS,
//   ICONS,
//   LOGOS,
//   SHAREDS,
//   DOCUMENTS
// } from '../../themes'

// Wrapper
// ============================================================
export const ButtonWrapper = styled.div`
  /* Parent
  ------------------------------- */
  transition: ${VARIABLES.TRANSITIONS.DEFAULT};
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;

  /* Childrens
  ------------------------------- */
  .button-text {
    /* Font size */
    ${UTILITIES.FONT_SIZES()};

    /* Text color */
    ${UTILITIES.TEXT_COLORS()};

    font-family: ${VARIABLES.FONT_FAMILIES.FIRST_REGULAR};
  }

  .button-icon {
    &.is-front {
      margin-right: 10px;
    }

    &.is-back {
      margin-left: 10px;
    }
  }

  /* Modifiers
  ------------------------------- */
  /* Background color */
  ${UTILITIES.BACKGROUND_COLORS()};

  /* Border width */
  ${UTILITIES.BORDER_WIDTHS()};

  /* Border color */
  ${UTILITIES.BORDER_COLORS()};

  /* Border radius */
  ${UTILITIES.BORDER_RADIUSES()};

  /* UI */
  &.is-ui-border {
    border-style: solid;
  }

  &.is-ui-disabled {
    pointer-events: none;
    background-color: ${VARIABLES.COLORS.TEXT_PLACEHOLDER};
  }

  /* Width */
  &.is-width-xs {
    width: ${VARIABLES.BUTTON.WIDTHS.XS};
  }

  &.is-width-sm {
    width: ${VARIABLES.BUTTON.WIDTHS.SM};
  }

  &.is-width-md {
    width: ${VARIABLES.BUTTON.WIDTHS.MD};
  }

  &.is-width-lg {
    width: ${VARIABLES.BUTTON.WIDTHS.LG};
  }

  &.is-width-xl {
    width: ${VARIABLES.BUTTON.WIDTHS.XL};
  }

  /* Height */
  &.is-height-xs {
    height: ${VARIABLES.BUTTON.HEIGHTS.XS};
  }

  &.is-height-sm {
    height: ${VARIABLES.BUTTON.HEIGHTS.SM};
  }

  &.is-height-md {
    height: ${VARIABLES.BUTTON.HEIGHTS.MD};
  }

  &.is-height-lg {
    height: ${VARIABLES.BUTTON.HEIGHTS.LG};
  }

  &.is-height-xl {
    height: ${VARIABLES.BUTTON.HEIGHTS.XL};
  }

  /* Media queries
  ------------------------------- */
`
