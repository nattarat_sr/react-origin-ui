import React from 'react'
import ClassNames from 'classnames'
import PropTypes from 'prop-types'
import {
  ListWrapper
} from './styled'

class ListText extends React.PureComponent {
  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    ui: PropTypes.oneOf([
      'heading'
    ]),

    /**
    * Modifier name for change bullet spacing
    */
    bullet: PropTypes.oneOf([
      'number-one',
      'number-two',
      'icon-size-sm'
    ]),

    /**
    * Insert bullet eg. node, string
    */
    bulletItem: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ])
  }

  render () {
    const {
      className,
      children,
      ui,
      bullet,
      bulletItem
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const bulletClasses = ClassNames(bullet)
    const classes = ClassNames(
      'list-text',
      { [`is-ui-${uiClasses}`]: uiClasses },
      { [`is-bullet-${bulletClasses}`]: bulletClasses },
      className
    )

    return (
      <div className={classes}>
        {
          bullet &&
            <div className='list-bullet'>
              {bulletItem}
            </div>
        }
        {children}
      </div>
    )
  }
}

/**
 * List description:
 * - list item content eg. term conditions and privacy
 */

export class List extends React.PureComponent {
  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([]),

    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ])
  }

  static Text = ListText

  render () {
    const {
      ui,
      className,
      children
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const classes = ClassNames(
      'list',
      { [`is-ui-${uiClasses}`]: uiClasses },
      className
    )

    return (
      <ListWrapper
        className={classes}
      >
        {children}
      </ListWrapper>
    )
  }
}
