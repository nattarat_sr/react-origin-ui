Default:

```jsx
<Notification
  srcIconType={require('./images/icon-info-black.png')}
  srcIconClose={require('./images/icon-close-black.png')}
  message='Default notification!!!'
/>
<div style={{
  padding: '15px',
  marginTop: '15px',
  backgroundColor: '#EEEEEE'
}}>
  Sample react-toastify usage: https://gist.github.com/Nattarat/c8329ff1ca82f12a111964d7e341fe19#file-react-toastify-js
</div>
```

Success:

```jsx
<Notification
  status='success'
  message='Success notification!!!'
/>
<div style={{
  padding: '15px',
  marginTop: '15px',
  backgroundColor: '#EEEEEE'
}}>
  Sample react-toastify usage: https://gist.github.com/Nattarat/c8329ff1ca82f12a111964d7e341fe19#file-react-toastify-js
</div>
```

Error:

```jsx
<Notification
  status='error'
  message='Error notification!!!'
/>
<div style={{
  padding: '15px',
  marginTop: '15px',
  backgroundColor: '#EEEEEE'
}}>
  Sample react-toastify usage: https://gist.github.com/Nattarat/c8329ff1ca82f12a111964d7e341fe19#file-react-toastify-js
</div>
```
