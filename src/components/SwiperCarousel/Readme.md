Default:

```jsx
<SwiperCarousel>
  <SwiperCarousel.Item>
    <img src={require('./images/sample-banner-1.jpg')} alt='Banner' />
  </SwiperCarousel.Item>
  <SwiperCarousel.Item>
    <img src={require('./images/sample-banner-2.jpg')} alt='Banner' />
  </SwiperCarousel.Item>
  <SwiperCarousel.Item>
    <img src={require('./images/sample-banner-3.jpg')} alt='Banner' />
  </SwiperCarousel.Item>
</SwiperCarousel>
```

Auto slides per view:

```jsx
<SwiperCarousel ui='card'
  options={{
    slidesPerView: 'auto'
  }}
>
  <SwiperCarousel.Item>
    <div style={{ height: '200px', backgroundColor: 'red' }}>Item</div>
  </SwiperCarousel.Item>
  <SwiperCarousel.Item>
    <div style={{ height: '200px', backgroundColor: 'green' }}>Item</div>
  </SwiperCarousel.Item>
  <SwiperCarousel.Item>
    <div style={{ height: '200px', backgroundColor: 'blue' }}>Item</div>
  </SwiperCarousel.Item>
  <SwiperCarousel.Item>
    <div style={{ height: '200px', backgroundColor: 'purple' }}>Item</div>
  </SwiperCarousel.Item>
  <SwiperCarousel.Item>
    <div style={{ height: '200px', backgroundColor: 'brown' }}>Item</div>
  </SwiperCarousel.Item>
  <SwiperCarousel.Item>
    <div style={{ height: '200px', backgroundColor: 'pink' }}>Item</div>
  </SwiperCarousel.Item>
</SwiperCarousel>
```