import styled from 'styled-components'
import {
  default as VARIABLES
} from '../../themes/styles/bases/variables'
// import {
//   default as TYPOGRAPHYS
// } from '../../themes/styles/bases/typographys'
// import {
//   default as MIXINS
// } from '../../themes/styles/helpers/mixins'
import {
  default as UTILITIES
} from '../../themes/styles/helpers/utilities'
// import {
//   // Example using:
//   // background: url(${CONTENTS['image-sample.svg']});
//   CONTENTS,
//   ICONS,
//   LOGOS,
//   SHAREDS,
//   DOCUMENTS
// } from '../../themes'

// Wrapper
// ============================================================
export const ContainerWrapper = styled.div`
  /* Parent
  ------------------------------- */
  display: flex;
  /* flex-direction: column; */
  /* flex-wrap: wrap; */
  width: 100%;
  margin-left: auto;
  margin-right: auto;

  /* Childrens
  ------------------------------- */
  .container-content {}

  /* Modifiers
  ------------------------------- */
  /* Flex */
  ${UTILITIES.FLEX_DIRECTIONS()};
  ${UTILITIES.FLEX_WRAPS()};
  ${UTILITIES.FLEX_ALIGNS()};
  ${UTILITIES.FLEX_ALIGN_ITEMS()};

  /* Width */
  &.is-width-mobile {
    max-width: ${VARIABLES.BREAKPOINTS.MOBILE_LG};
  }

  &.is-width-tablet {
    max-width: ${VARIABLES.BREAKPOINTS.TABLET_LG};
  }

  &.is-width-desktop {
    max-width: ${VARIABLES.BREAKPOINTS.LAPTOP_MD};
  }

  /* Height */
  &.is-height-fluid {
    height: 100%;
  }

  &.is-height-viewport {
    height: 100vh;
  }

  &.is-height-viewport-half {
    height: 50vh;
  }

  /* Padding */
  &.is-padding-content {
    padding-left: 30px;
    padding-right: 30px;
  }

  &.is-padding-content-mobile {
    @media (max-width: ${VARIABLES.BREAKPOINTS.MOBILE_LG_MAX}) {
      padding-left: 30px;
      padding-right: 30px;
    }
  }

  &.is-padding-content-mobile-phablet {
    @media (max-width: ${VARIABLES.BREAKPOINTS.PHABLET_LG_MAX}) {
      padding-left: 30px;
      padding-right: 30px;
    }
  }

  &.is-padding-content-mobile-tablet {
    @media (max-width: ${VARIABLES.BREAKPOINTS.TABLET_LG_MAX}) {
      padding-left: 30px;
      padding-right: 30px;
    }
  }

  /* Media queries
  ------------------------------- */
`
