Default:

```jsx
<Container>
  <div style={{ backgroundColor: 'gray', width: '100%' }}>Default</div>
</Container>
<br />
<Container>
  <Container.Content>
    <div style={{ backgroundColor: 'gray', width: '100%' }}>Default content</div>
  </Container.Content>
</Container>
```

Width:

```jsx
<Container width='mobile'>
  <div style={{ backgroundColor: 'gray', width: '100%' }}>Max width mobile - 414px</div>
</Container>
<br />
<Container width='tablet'>
  <div style={{ backgroundColor: 'gray', width: '100%' }}>Max width tablet - 1024px</div>
</Container>
<br />
<Container width='desktop'>
  <div style={{ backgroundColor: 'gray', width: '100%' }}>Max width desktop - 1440px</div>
</Container>
```

Height:

```jsx
<Container height='viewport-half'>
  <div style={{ backgroundColor: 'gray', width: '100%', height: '100%' }}>Half viewport height</div>
</Container>
```

Padding:

```jsx
<Container padding='content-mobile'>
  <div style={{ backgroundColor: 'gray', width: '100%' }}>Left-right padding for content at mobile screen size</div>
</Container>
```

Flex aligns:

```jsx
<Container flexDirection='row' flexAlign='right'>
  <Container.Content>
    <div style={{ backgroundColor: 'gray' }}>Right content</div>
  </Container.Content>
</Container>
<br />
<Container flexDirection='row' flexAlign='center'>
  <Container.Content>
    <div style={{ backgroundColor: 'gray' }}>Center content</div>
  </Container.Content>
</Container>
<br />
<Container flexDirection='row' flexAlign='spacebetween'>
  <Container.Content>
    <div style={{ backgroundColor: 'gray' }}>SpaceBetween - Left content</div>
  </Container.Content>
  <Container.Content>
    <div style={{ backgroundColor: 'gray' }}>SpaceBetween - Right content</div>
  </Container.Content>
</Container>
```

Flex align items:

```jsx
<Container flexDirection='row' flexAlign='spacebetween' flexAlignItems='middle'>
  <Container.Content>
    <div style={{ backgroundColor: 'gray', width: '100px', height: '100px' }}>Content</div>
  </Container.Content>
  <Container.Content>
    <div style={{ backgroundColor: 'gray' }}>Middle content</div>
  </Container.Content>
</Container>
<br />
<Container flexDirection='row' flexAlign='spacebetween' flexAlignItems='bottom'>
  <Container.Content>
    <div style={{ backgroundColor: 'gray', width: '100px', height: '100px' }}>Content</div>
  </Container.Content>
  <Container.Content>
    <div style={{ backgroundColor: 'gray' }}>Bottom content</div>
  </Container.Content>
</Container>
```

Top - Bottom content:

```jsx
<Container height='viewport-half' flexAlign='spacebetween-vertical'>
  <Container.Content>
    <div style={{ backgroundColor: 'gray' }}>Top content</div>
  </Container.Content>
  <Container.Content>
    <div style={{ backgroundColor: 'gray' }}>Bottom content</div>
  </Container.Content>
</Container>
```
