import React from 'react'
import ClassNames from 'classnames'
import PropTypes from 'prop-types'
import {
  ContainerWrapper
} from './styled'

class ContainerContent extends React.PureComponent {
  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([])
  }

  render () {
    const {
      className,
      children,
      ui
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const classes = ClassNames(
      'container-content',
      { [`is-ui-${uiClasses}`]: uiClasses },
      className
    )

    return (
      <div className={classes}>
        {children}
      </div>
    )
  }
}

/**
 * Container description:
 * - control site width
 */

export class Container extends React.PureComponent {
  static defaultProps = {
    flexDirection: 'column',
    flexWrap: 'wrap'
  }

  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([]),

    /**
    * Width
    */
    width: PropTypes.oneOf([
      'mobile',
      'tablet',
      'desktop'
    ]),

    /**
    * Height
    */
    height: PropTypes.oneOf([
      'fluid',
      'viewport',
      'viewport-half'
    ]),

    /**
    * Padding
    */
    padding: PropTypes.oneOf([
      'content',
      'content-mobile',
      'content-mobile-phablet',
      'content-mobile-tablet'
    ]),

    /**
    * Flex directions
    */
    flexDirection: PropTypes.oneOf([
      'row',
      'row-reverse',
      'column',
      'column-reverse'
    ]),

    /**
    * Flex wraps
    */
    flexWrap: PropTypes.oneOf([
      'wrap',
      'nowrap'
    ]),

    /**
    * Flex row/column aligns
    */
    flexAlign: PropTypes.oneOf([
      'left',
      'right',
      'center',
      'spacebetween',
      'top',
      'bottom',
      'middle',
      'spacebetween-vertical'
    ]),

    /**
    * Flex align items
    */
    flexAlignItems: PropTypes.oneOf([
      'top',
      'bottom',
      'middle'
    ])
  }

  static Content = ContainerContent

  render () {
    const {
      className,
      children,
      ui,
      width,
      height,
      padding,
      flexDirection,
      flexWrap,
      flexAlign,
      flexAlignItems
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const widthClasses = ClassNames(width)
    const heightClasses = ClassNames(height)
    const paddingClasses = ClassNames(padding)
    const flexDirectionClasses = ClassNames(flexDirection)
    const flexWrapClasses = ClassNames(flexWrap)
    const flexAlignClasses = ClassNames(flexAlign)
    const flexAlignItemsClasses = ClassNames(flexAlignItems)
    const classes = ClassNames(
      'container',
      { [`is-ui-${uiClasses}`]: uiClasses },
      { [`is-width-${widthClasses}`]: widthClasses },
      { [`is-height-${heightClasses}`]: heightClasses },
      { [`is-padding-${paddingClasses}`]: paddingClasses },
      { [`is-flexdirection-${flexDirectionClasses}`]: flexDirectionClasses },
      { [`is-flexwrap-${flexWrapClasses}`]: flexWrapClasses },
      { [`is-flexalign-${flexAlignClasses}`]: flexAlignClasses },
      { [`is-flexalign-vertical-${flexAlignItemsClasses}`]: flexAlignItemsClasses },
      className
    )

    return (
      <ContainerWrapper
        className={classes}
      >
        {children}
      </ContainerWrapper>
    )
  }
}
