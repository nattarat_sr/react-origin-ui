import styled from 'styled-components'
import {
  default as VARIABLES
} from '../../themes/styles/bases/variables'
import {
  default as TYPOGRAPHYS
} from '../../themes/styles/bases/typographys'
// import {
//   default as MIXINS
// } from '../../themes/styles/helpers/mixins'
// import {
//   default as UTILITIES
// } from '../../themes/styles/helpers/utilities'
// import {
//   // Example using:
//   // background: url(${CONTENTS['image-PaginationControl.svg']});
//   CONTENTS,
//   ICONS,
//   LOGOS,
//   SHAREDS,
//   DOCUMENTS
// } from '../../themes'

// Wrapper
// ============================================================
export const PaginationControlWrapper = styled.div`
  /* Parent
  ------------------------------- */
  /* Childrens
  ------------------------------- */
  .pagination-control-container {
    display: flex;
    align-items: center;

    .pagination-control-item {
      display: block;

      a {
        ${TYPOGRAPHYS.FONT_STYLES.FIRST_REGULAR_MN};
        transition: ${VARIABLES.TRANSITIONS.DEFAULT};
        display: flex;
        justify-content: center;
        align-items: center;
        width: 24px;
        height: 24px;
        background-color: ${VARIABLES.COLORS.WHITE};
        border: 1px solid ${VARIABLES.COLORS.GRAY_1};
        border-right-width: 0;
        color: ${VARIABLES.COLORS.TEXT_DETAIL};
      }

      &.is-first,
      &.is-prev,
      &.is-next,
      &.is-last {
        a {}
      }

      &.is-prev,
      &.is-next {
        position: relative;

        a {
          font-size: 0;

          &:before {
            content: ' ';
            display: block;
          }
        }

        /* Arrow */
        &:before,
        &:after {
          content: ' ';
          pointer-events: none;
          position: absolute;
          top: 50%;
          width: 0;
          height: 0;
          border: solid transparent;
          border-color: transparent;
        }

        &:before {
          border-width: 5px;
          margin-top: -5px;
        }

        &:after {
          border-width: 4px;
          margin-top: -4px;
        }
      }

      &.is-first,
      &.is-last {
        a {
          width: auto;
          padding-right: 5px;
          padding-left: 5px;
        }
      }

      &.is-prev {
        /* Arrow */
        &:before,
        &:after {
          right: 10px;
        }

        &:before {
          border-right-color: ${VARIABLES.COLORS.TEXT_HEAD};
        }

        &:after {
          border-right-color: ${VARIABLES.COLORS.WHITE};
        }
      }

      &.is-next {
        /* Arrow */
        &:before,
        &:after {
          left: 10px;
        }

        &:before {
          border-left-color: ${VARIABLES.COLORS.TEXT_HEAD};
        }

        &:after {
          border-left-color: ${VARIABLES.COLORS.WHITE};
        }
      }

      &.is-last {
        a {
          border-right-width: 1px;
        }
      }

      &.is-active {
        a {
          background-color: ${VARIABLES.COLORS.TEXT_DETAIL};
          color: ${VARIABLES.COLORS.WHITE};
        }
      }

      &.is-disabled {
        &.is-first,
        &.is-prev,
        &.is-next,
        &.is-last {
          a {
            pointer-events: none;
            color: ${VARIABLES.COLORS.GRAY_2};
          }
        }

        &.is-prev {
          &:before {
            border-right-color: ${VARIABLES.COLORS.GRAY_2};
          }
        }

        &.is-next {
          &:before {
            border-left-color: ${VARIABLES.COLORS.GRAY_2};
          }
        }
      }
    }
  }

  /* Modifiers
  ------------------------------- */
  /* Media queries
  ------------------------------- */
`
