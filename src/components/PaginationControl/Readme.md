Default:

```jsx
<div style={{
  padding: '15px',
  marginTop: '15px',
  backgroundColor: '#EEEEEE'
}}>
  Sample react-js-pagination usage, please see source file at:
  <br />
  - 'src/containers/Lab/Examples/Pagination/index.js'
  <br />
  - or 'npm start' and go to route '/lab/examples/pagination' for see live example
</div>
```
