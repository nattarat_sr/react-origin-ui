// //////////////////////////////////////////////////////////////////////////////////////////////////
// ==================================================================================================
//
// Variables:
//
// * Factors
// * Colors
// * Font families
// * Font sizes
// * Line heights
// * Letter spacing
// * Z indexs
// * Breakpoints
// * Transitions
// * Animation timings
// * Border widths
// * Border radiuses
// * Box shadows
// * Component width/height/padding/margin/others
//   - Button
//   - Icon
//   - Field
//   - Checkbox
//   - Modal
//   - Scrollbar
//   - Table
//   - Notification
//
// ==================================================================================================
// //////////////////////////////////////////////////////////////////////////////////////////////////

// Factors (using in variables)
// ============================================================
const SPACING_FACTOR = '5px'
const LINE_HEIGHT_FACTOR = '1.35'
// Font sizes
const FONT_SIZES_MN   = '12px'
const FONT_SIZES_TN   = '14px'
const FONT_SIZES_XXS  = '16px'
const FONT_SIZES_XS   = '18px'
const FONT_SIZES_SM   = '20px'
const FONT_SIZES_MD   = '24px'
const FONT_SIZES_LG   = '28px'
const FONT_SIZES_XL   = '32px'
const FONT_SIZES_XXL  = '36px'
const FONT_SIZES_BG   = '42px'
const FONT_SIZES_HG   = '48px'
const FONT_SIZES_MS   = '60px'

export default {
  // Factors
  // ============================================================
  FACTORS: {
    SPACING: SPACING_FACTOR,
    LINE_HEIGHT: LINE_HEIGHT_FACTOR
  },

  // Colors
  // ============================================================
  COLORS: {
    // Base
    // ------------------------------
    // Black
    BLACK: '#000000',
    // White
    WHITE: '#FFFFFF',
    // Gray
    GRAY_1: '#CCCCCC',
    GRAY_2: '#EEEEEE',
    // Red
    RED_1: '#DD4C4F',
    // Green
    GREEN_1: '#57ADB8',
    // Blue
    BLUE_1: '#4A94C9',

    // Role
    // ------------------------------
    // Text
    TEXT_HEAD: '#333333',
    TEXT_SUB_HEAD: '#666666',
    TEXT_DETAIL: '#999999',
    TEXT_LINK: '#006DC9',
    TEXT_PLACEHOLDER: '#AAAAAA',

    // Validation
    SYSTEM_ERROR: '#C01713',
    SYSTEM_SUCCESS: '#34BC73',

    // Other
    CALL_TO_ACTION: '#FF8F00',

    // Overlay
    OVERLAY_1: 'rgba(0, 0, 0, 0.75)'

    // Specific (for unique color in design)
    // ------------------------------
  },

  // Font families
  // ============================================================
  FONT_FAMILIES: {
    FIRST_REGULAR:  'Prompt-Regular',
    FIRST_MEDIUM:   'Prompt-Medium',
    FIRST_BOLD:     'Prompt-Bold'
  },

  // Font sizes
  // ============================================================
  FONT_SIZES: {
    MN:   FONT_SIZES_MN,  // 12px
    TN:   FONT_SIZES_TN,  // 14px
    XXS:  FONT_SIZES_XXS, // 16px
    XS:   FONT_SIZES_XS,  // 18px
    SM:   FONT_SIZES_SM,  // 20px
    MD:   FONT_SIZES_MD,  // 24px
    LG:   FONT_SIZES_LG,  // 28px
    XL:   FONT_SIZES_XL,  // 32px
    XXL:  FONT_SIZES_XXL, // 36px
    BG:   FONT_SIZES_BG,  // 42px
    HG:   FONT_SIZES_HG,  // 48px
    MS:   FONT_SIZES_MS   // 60px
  },

  // Line heights
  // ============================================================
  LINE_HEIGHTS: {
    MN:   `calc(${LINE_HEIGHT_FACTOR} * ${FONT_SIZES_MN})`,   // 16.2px
    TN:   `calc(${LINE_HEIGHT_FACTOR} * ${FONT_SIZES_TN})`,   // 18.9px
    XXS:  `calc(${LINE_HEIGHT_FACTOR} * ${FONT_SIZES_XXS})`,  // 21.6px
    XS:   `calc(${LINE_HEIGHT_FACTOR} * ${FONT_SIZES_XS})`,   // 24.3px
    SM:   `calc(${LINE_HEIGHT_FACTOR} * ${FONT_SIZES_SM})`,   // 27px
    MD:   `calc(${LINE_HEIGHT_FACTOR} * ${FONT_SIZES_MD})`,   // 32.4px
    LG:   `calc(${LINE_HEIGHT_FACTOR} * ${FONT_SIZES_LG})`,   // 37.8px
    XL:   `calc(${LINE_HEIGHT_FACTOR} * ${FONT_SIZES_XL})`,   // 43.2px
    XXL:  `calc(${LINE_HEIGHT_FACTOR} * ${FONT_SIZES_XXL})`,  // 48.6px
    BG:   `calc(${LINE_HEIGHT_FACTOR} * ${FONT_SIZES_BG})`,   // 56.7px
    HG:   `calc(${LINE_HEIGHT_FACTOR} * ${FONT_SIZES_HG})`,   // 64.8px
    MS:   `calc(${LINE_HEIGHT_FACTOR} * ${FONT_SIZES_MS})`    // 81px
  },

  // Letter spacing
  // ============================================================
  LETTER_SPACINGS: {
    // MN:   'px',
    // TN:   'px',
    // XXS:  'px',
    // XS:   'px',
    // SM:   'px',
    // MD:   'px',
    // LG:   'px',
    // XL:   'px',
    // XXL:  'px',
    // BG:   'px',
    // HG:   'px',
    // MS:   'px'
  },

  // Zindexs
  // ============================================================
  Z_INDEXS: {
    LV_1: '1',
    LV_2: '9',
    LV_3: '10',
    LV_4: '11',
    LV_5: '99',
    LV_6: '100',
    LV_7: '101',
    LV_8: '999',
    LV_9: '1000',
    LV_10: '1001'
  },

  // Breakpoints
  // ============================================================
  BREAKPOINTS: {
    // Mobile
    MOBILE_XS: '320px',
    MOBILE_SM: '360px',
    MOBILE_MD: '375px',
    MOBILE_LG: '414px',
    // Max
    MOBILE_XS_MAX: '359px',
    MOBILE_SM_MAX: '374px',
    MOBILE_MD_MAX: '413px',
    MOBILE_LG_MAX: '479px',
    // Min
    MOBILE_XS_MIN: '321px',
    MOBILE_SM_MIN: '361px',
    MOBILE_MD_MIN: '376px',
    MOBILE_LG_MIN: '415px',

    // Phablet
    PHABLET_XS: '480px',
    PHABLET_SM: '640px',
    PHABLET_MD: '667px',
    PHABLET_LG: '736px',
    // Max
    PHABLET_XS_MAX: '639px',
    PHABLET_SM_MAX: '665px',
    PHABLET_MD_MAX: '735px',
    PHABLET_LG_MAX: '767px',
    // Min
    PHABLET_XS_MIN: '481px',
    PHABLET_SM_MIN: '641px',
    PHABLET_MD_MIN: '668px',
    PHABLET_LG_MIN: '737px',

    // Tablet
    TABLET_XS: '768px',
    TABLET_SM: '812px',
    TABLET_MD: '896px',
    TABLET_LG: '1024px',
    // Max
    TABLET_XS_MAX: '811px',
    TABLET_SM_MAX: '895px',
    TABLET_MD_MAX: '1023px',
    TABLET_LG_MAX: '1279px',
    // Min
    TABLET_XS_MIN: '769px',
    TABLET_SM_MIN: '813px',
    TABLET_MD_MIN: '897px',
    TABLET_LG_MIN: '1025px',

    // Laptop
    LAPTOP_XS: '1280px',
    LAPTOP_SM: '1366px',
    LAPTOP_MD: '1440px',
    LAPTOP_LG: '1600px',
    // Max
    LAPTOP_XS_MAX: '1365px',
    LAPTOP_SM_MAX: '1439px',
    LAPTOP_MD_MAX: '1599px',
    LAPTOP_LG_MAX: '1919px',
    // Min
    LAPTOP_XS_MIN: '1281px',
    LAPTOP_SM_MIN: '1367px',
    LAPTOP_MD_MIN: '1441px',
    LAPTOP_LG_MIN: '1601px',

    // Desktop
    DESKTOP_XS: '1920px',
    DESKTOP_SM: '2560px',
    // Max
    DESKTOP_XS_MAX: '2559px',
    // Min
    DESKTOP_XS_MIN: '1921px',
    DESKTOP_SM_MIN: '2561px'
  },

  // Transitions
  // ============================================================
  TRANSITIONS: {
    DEFAULT: 'all 0.3s ease'
  },

  // Animation timings
  // ============================================================
  ANIMATION_TIMINGS: {
    ELASTIC: 'cubic-bezier(.835, -.005, .06, 1)'
  },

  // Border widths
  // ============================================================
  BORDER_WIDTHS: {
    MN:   '1px',
    TN:   '2px',
    XXS:  '3px',
    XS:   '4px',
    SM:   '5px',
    MD:   '6px',
    LG:   '7px',
    XL:   '8px',
    XXL:  '9px',
    BG:   '10px',
    HG:   '15px',
    MS:   '20px'
  },

  // Border radiuses
  // ============================================================
  BORDER_RADIUSES: {
    MN:   '2px',
    TN:   '4px',
    XXS:  '6px',
    XS:   '8px',
    SM:   '10px',
    MD:   '12px',
    LG:   '14px',
    XL:   '16px',
    XXL:  '18px',
    BG:   '20px',
    HG:   '40px',
    MS:   '60px'
  },

  // Box shadows
  // ============================================================
  BOX_SHADOWS: {
    SHADOW_1: '0 5px 5px rgba(0, 0, 0, .5)'
  },

  // Component width/height/others
  // ============================================================
  BUTTON: {
    // Width
    WIDTHS: {
      // MN:   'px',
      // TN:   'px',
      // XXS:  'px',
      XS:   '100px',
      SM:   '125px',
      MD:   '150px',
      LG:   '200px',
      XL:   '250px',
      // XXL:  'px',
      // BG:   'px',
      // HG:   'px',
      // MS:   'px'
    },

    // Height
    HEIGHTS: {
      // MN:   'px',
      // TN:   'px',
      // XXS:  'px',
      XS:   '24px',
      SM:   '32px',
      MD:   '48px',
      LG:   '60px',
      XL:   '72px',
      // XXL:  'px',
      // BG:   'px',
      // HG:   'px',
      // MS:   'px'
    }
  },

  ICON: {
    // Size meaning is combine to width/height
    SIZES: {
      MN:   '10px',
      TN:   '12px',
      XXS:  '14px',
      XS:   '16px',
      SM:   '18px',
      MD:   '24px',
      LG:   '28px',
      XL:   '32px',
      XXL:  '36px',
      BG:   '42px',
      HG:   '48px',
      MS:   '60px'
    }
  },

  FIELD: {
    // Width
    WIDTHS: {
      // MN:   'px',
      // TN:   'px',
      // XXS:  'px',
      XS:   '200px',
      SM:   '250px',
      MD:   '350px',
      LG:   '450px',
      // XL:   'px',
      // XXL:  'px',
      // BG:   'px',
      // HG:   'px',
      // MS:   'px'
    },

    // Label width
    LABEL_WIDTHS: {
      // MN:   'px',
      // TN:   'px',
      // XXS:  'px',
      // XS:   'px',
      SM:   '100px',
      MD:   '150px',
      LG:   '200px',
      // XL:   'px',
      // XXL:  'px',
      // BG:   'px',
      // HG:   'px',
      // MS:   'px'
    },

    // Border radius
    BORDER_RADIUS: '5px',

    // Type
    TYPES: {
      TEXT: {
        HEIGHT: '30px'
      },
      TEXTAREA: {
        HEIGHT: '90px'
      }
    }
  },

  CHECKBOX: {
    WIDTH: '16px',
    HEIGHT: '16px',
    BORDER_RADIUS: '3px',
    ICON: {
      WIDTH: '10px',
      HEIGHT: '7px'
    }
  },

  MODAL: {
    // Width
    WIDTHS: {
      // MN:   'px',
      // TN:   'px',
      // XXS:  'px',
      // XS:   'px',
      SM: '320px',
      MD: '414px',
      LG: '768px',
      // XL:   'px',
      // XXL:  'px',
      // BG:   'px',
      // HG:   'px',
      // MS:   'px'
    }
  },

  SCROLLBAR: {
    WIDTH: '8px',
    HEIGHT: '8px',
    BORDER_RADIUS: '8px',
    THUMB: {
      COLORS: {
        DEFAULT: '#B5B5B5',
        HOVER: '#858585',
        ACTIVE: '#858585'
      }
    },
    TRACK: {
      COLORS: {
        DEFAULT: '#E8E8E8',
        HOVER: '#E8E8E8',
        ACTIVE: '#E8E8E8'
      }
    },
  },

  TABLE: {
    COLUMN: {
      // Width
      WIDTHS: {
        MN:   '50px',
        TN:   '75px',
        XXS:  '100px',
        XS:   '125px',
        SM:   '150px',
        MD:   '175px',
        LG:   '200px',
        XL:   '225px',
        XXL:  '250px',
        BG:   '300px',
        HG:   '325px',
        MS:   '350px'
      },
    }
  },

  NOTIFICATION: {
    // Width
    WIDTHS: {
      // MN:   'px',
      // TN:   'px',
      // XXS:  'px',
      // XS:   'px',
      SM: '375px',
      MD: '480px',
      LG: '768px',
      // XL:   'px',
      // XXL:  'px',
      // BG:   'px',
      // HG:   'px',
      // MS:   'px'
    },

    ICON: {
      WIDTHS: {
        // MN:   'px',
        // TN:   'px',
        // XXS:  'px',
        // XS:   'px',
        SM: '18px',
        MD: '24px',
        LG: '30px',
        // XL:   'px',
        // XXL:  'px',
        // BG:   'px',
        // HG:   'px',
        // MS:   'px'
      }
    },

    BUTTON_CLOSE: {
      WIDTH: '45px'
    }
  },

  SORTABLE: {
    GUTTER_WIDTHS: {
      MN:   '5px',
      TN:   '10px',
      XXS:  '15px',
      XS:   '20px',
      SM:   '25px',
      MD:   '30px',
      LG:   '35px',
      XL:   '40px',
      XXL:  '45px',
      BG:   '50px',
      HG:   '55px',
      MS:   '60px'
    }
  }
}
