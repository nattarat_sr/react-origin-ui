import apiService from '../apiService'
import { BASE_API, BASE_PATH_API } from "../apiConfig";

const apiPath = BASE_API + BASE_PATH_API + '/code'

export const couponService = {
    barcode: (params) => {
        return apiService.get(`${apiPath}/${params}`)
    }
}